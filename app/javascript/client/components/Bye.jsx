import React from 'react';
import PropTypes from 'prop-types';

const Bye = ({name}) =>
  <div>Bye bye, {name}!</div>

Bye.propTypes = {
  name: PropTypes.string
}

export default Bye;
