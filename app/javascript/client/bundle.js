import WebpackerReact from 'webpacker-react';

import Hello from './components/Hello';
import Bye   from './components/Bye';

WebpackerReact.setup({
  Hello,
  Bye,
});
